package eu.millibit.bluetootherror.statemachine;

import android.bluetooth.BluetoothAdapter;
import android.support.annotation.NonNull;

import eu.millibit.bluetootherror.R;

class TurningAdaptorOn extends BaseState {
    TurningAdaptorOn(IState idleState) {
        super(idleState);
    }

    @Override
    public int getStatusText() {
        return R.string.turning_adaptor_on;
    }

    @Override
    public void onEntry(@NonNull IEngine engine) {
        super.onEntry(engine);

        if(engine.getAdaptorState() == BluetoothAdapter.STATE_ON)
            engine.setState(idleState);
        else
            engine.enableAdaptor();
    }

    @Override
    public void onAdaptorStateChanged(int newState) {
        super.onAdaptorStateChanged(newState);

        if (engine != null && newState == BluetoothAdapter.STATE_ON)
            engine.setState(idleState);
    }
}
