package eu.millibit.bluetootherror.statemachine;

import android.bluetooth.BluetoothGatt;
import android.support.annotation.StringRes;

import eu.millibit.bluetootherror.R;

public class ConnectedState extends BaseState {
    private final IState disconnectingState;

    ConnectedState(IState idleState) {
        super(idleState);

        disconnectingState = new DisconnectingState(idleState);
    }

    @Override
    public boolean isStimulusEnabled(int elementID) {
        switch (elementID) {
            case UI_ELEMENT_DISCONNECT_BUTTON:
                return true;
            default:
                return super.isStimulusEnabled(elementID);
        }
    }

    @Override
    public boolean isConnected() {
        return true;
    }

    @Override
    public void onStimulus(int elementID) {
        switch (elementID) {
            case UI_ELEMENT_DISCONNECT_BUTTON:
                stopAsync();
                break;

            default:
                super.onStimulus(elementID);
                break;
        }
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        switch (newState) {
            case BluetoothGatt.STATE_DISCONNECTED:
                if(engine != null) {
                    engine.disposeConnection();
                    engine.setState(idleState);
                }
                break;

            default:
                //FIXME: How to log this unsupported event
                break;
        }
    }

    @Override
    public void stopAsync() {
        super.stopAsync();
        if(engine != null) {
            engine.setState(disconnectingState);
        }
    }

    @Override
    @StringRes
    public int getStatusText() {
        return R.string.connected;
    }
}
