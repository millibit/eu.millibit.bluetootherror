package eu.millibit.bluetootherror.statemachine;

import android.bluetooth.BluetoothGatt;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import eu.millibit.bluetootherror.R;

public class DisconnectingState extends BaseState {
    DisconnectingState(IState idleState) {
        super(idleState);
    }

    @Override
    @StringRes
    public int getStatusText() {
        return R.string.disconnecting;
    }

    @Override
    public void onEntry(@NonNull IEngine engine) {
        super.onEntry(engine);
        engine.disconnect();
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        switch (newState) {
            case BluetoothGatt.STATE_DISCONNECTED:
                if(engine != null) {
                    engine.disposeConnection();
                    engine.setState(idleState);
                }
                break;

            default:
                //FIXME: How to log this unsupported event
                break;
        }
    }
}
