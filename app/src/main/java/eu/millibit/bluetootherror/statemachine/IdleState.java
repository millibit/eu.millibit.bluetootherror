package eu.millibit.bluetootherror.statemachine;

import android.support.annotation.StringRes;

import eu.millibit.bluetootherror.R;

public class IdleState extends BaseState {
    private IState startingClassicScanState = new StartingClassicScanState(this);
    private IState bleScanningState = new BLEScanningState(this);
    private IState connectingState = new ConnectingState(this);
    private IState cycleAdaptor = new TurningAdaptorOff(this);

    public IdleState() {
        super(null);
    }

    @Override
    public boolean isStimulusEnabled(int elementID) {
        switch (elementID) {
            case UI_ELEMENT_CONNECT_BUTTON:
            case UI_ELEMENT_START_CLASSIC_SCAN_BUTTON:
            case UI_ELEMENT_START_BLE_SCAN_BUTTON:
            case UI_ELEMENT_ADAPTOR_CYCLE_BUTTON:
                return true;

            default:
                return super.isStimulusEnabled(elementID);
        }
    }

    @Override
    public void onStimulus(int elementID) {
        switch (elementID) {
            case UI_ELEMENT_CONNECT_BUTTON:
                if(engine != null)
                    engine.setState(connectingState);
                break;

            case UI_ELEMENT_START_CLASSIC_SCAN_BUTTON:
                if(engine != null)
                    engine.setState(startingClassicScanState);
                break;

            case UI_ELEMENT_START_BLE_SCAN_BUTTON:
                if(engine != null)
                    engine.setState(bleScanningState);
                break;

            case UI_ELEMENT_ADAPTOR_CYCLE_BUTTON:
                if(engine != null)
                    engine.setState(cycleAdaptor);
                break;
        }
    }

    @Override
    @StringRes
    public int getStatusText() {
        return R.string.idle;
    }
}
