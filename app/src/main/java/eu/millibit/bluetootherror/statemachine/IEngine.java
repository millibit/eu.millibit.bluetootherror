package eu.millibit.bluetootherror.statemachine;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import eu.millibit.bluetootherror.Device;

public interface IEngine {
    // Classic scan functions
    void startClassicScan();
    void stopClassicScan();

    // BLE scan functions
    void startBLEScan();
    void stopBLEScan();

    // Adapter functions
    void enableAdaptor();
    void disableAdaptor();
    int getAdaptorState();

    // Connect functions
    void connect(@NonNull String address, boolean autoConnect);
    void disconnect();
    void disposeConnection();

    void clearDeviceList();

    void setState(IState newState);

    Device deviceFromAddress(@NonNull String address);

    @Nullable Device getSelectedDevice();

    void onDeviceChanged(@NonNull Device device);

    void postDelayed(Runnable r, int milliseconds);
}
