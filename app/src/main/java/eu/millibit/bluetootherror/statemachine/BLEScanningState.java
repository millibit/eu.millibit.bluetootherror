package eu.millibit.bluetootherror.statemachine;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import eu.millibit.bluetootherror.Device;
import eu.millibit.bluetootherror.R;

public class BLEScanningState extends BaseState {
    BLEScanningState(IState idleState) {
        super(idleState);
    }

    @Override
    public boolean isStimulusEnabled(int elementID) {
        switch (elementID) {
            case UI_ELEMENT_STOP_BLE_SCAN_BUTTON:
                return true;
            default:
                return super.isStimulusEnabled(elementID);
        }
    }

    @Override
    public void onStimulus(int elementID) {
        switch (elementID) {
            case UI_ELEMENT_STOP_BLE_SCAN_BUTTON:
                stopAsync();
                break;

            default:
                super.onStimulus(elementID);
                break;
        }
    }


    @Override
    public void onEntry(@NonNull IEngine engine) {
        super.onEntry(engine);
        engine.clearDeviceList();
        engine.startBLEScan();
    }

    @Override
    public void stopAsync() {
        super.stopAsync();
        if(engine != null) {
            engine.stopBLEScan();
            engine.setState(idleState);
        }
    }

    @Override
    public void onBLEDiscovery(BluetoothDevice bluetoothDevice, int rssi) {
        if(engine != null && bluetoothDevice.getName() != null) {
            Device device = engine.deviceFromAddress(bluetoothDevice.getAddress());
            device.setRSSI(rssi);
            device.setName(bluetoothDevice.getName());
            engine.onDeviceChanged(device);
        }
    }

    @Override
    @StringRes
    public int getStatusText() {
        return R.string.ble_scanning;
    }
}
