package eu.millibit.bluetootherror.statemachine;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import eu.millibit.bluetootherror.R;

public class StartingClassicScanState extends BaseState {

    private IState scanningState;
    private IState stoppingState;

    StartingClassicScanState(IState idleState) {
        super(idleState);
        stoppingState = new StoppingClassicScanState(idleState);
        scanningState = new ClassicScanState(idleState, stoppingState);
    }

    @Override
    public boolean isStimulusEnabled(int elementID) {
        switch (elementID) {
            case UI_ELEMENT_STOP_CLASSIC_SCAN_BUTTON:
                return true;

            default:
                return super.isStimulusEnabled(elementID);
        }
    }

    @Override
    public void onStimulus(int elementID) {
        switch (elementID) {
            case UI_ELEMENT_STOP_CLASSIC_SCAN_BUTTON:
                stopAsync();
                break;

            default:
                super.onStimulus(elementID);
                break;
        }
    }


    @Override
    public void stopAsync() {
        super.stopAsync();
        if(engine != null)
            engine.setState(stoppingState);
    }

    @Override
    public void onClassicDiscoveryStarted() {
        if(engine != null)
            engine.setState(scanningState);
    }

    @Override
    @StringRes
    public int getStatusText() {
        return R.string.starting_classic_scan;
    }

    @Override
    public void onEntry(@NonNull IEngine engine) {
        super.onEntry(engine);
        engine.clearDeviceList();
        engine.startClassicScan();
    }
}
