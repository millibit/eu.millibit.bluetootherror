package eu.millibit.bluetootherror.statemachine;

import android.bluetooth.BluetoothGatt;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import eu.millibit.bluetootherror.Device;
import eu.millibit.bluetootherror.R;

public class ConnectingState extends BaseState {
    private final IState connectedState;

    ConnectingState(IState idleState) {
        super(idleState);

        connectedState = new ConnectedState(idleState);
    }

    @Override
    public boolean isStimulusEnabled(int elementID) {
        switch (elementID) {
            case UI_ELEMENT_DISCONNECT_BUTTON:
                return true;
            default:
                return super.isStimulusEnabled(elementID);
        }
    }

    @Override
    public void onStimulus(int elementID) {
        switch (elementID) {
            case UI_ELEMENT_DISCONNECT_BUTTON:
                stopAsync();
                break;

            default:
                super.onStimulus(elementID);
                break;
        }
    }


    @Override
    public void onEntry(@NonNull IEngine engine) {
        super.onEntry(engine);

        Device device = engine.getSelectedDevice();
        if(device != null)
            engine.connect(device.getAddress(), false);
        else
            engine.setState(idleState);
    }


    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        switch (newState) {
            case BluetoothGatt.STATE_CONNECTED:
                if(engine != null)
                    engine.setState(connectedState);
                break;

            case BluetoothGatt.STATE_DISCONNECTED:
                if(engine != null) {
                    engine.disposeConnection();
                    engine.setState(idleState);
                }
                break;

            default:
                //FIXME: How to log this unsupported event
                break;
        }
    }

    @Override
    public void stopAsync() {
        super.stopAsync();
        if(engine != null) {
            engine.disconnect();
            engine.disposeConnection();
            engine.setState(idleState);
        }
    }

    @Override
    @StringRes
    public int getStatusText() {
        return R.string.connecting;
    }
}
