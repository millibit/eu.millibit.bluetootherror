package eu.millibit.bluetootherror.statemachine;

import android.bluetooth.BluetoothDevice;
import android.support.annotation.StringRes;

import eu.millibit.bluetootherror.Device;
import eu.millibit.bluetootherror.R;

public class ClassicScanState extends BaseState {

    private final IState stoppingState;

    ClassicScanState(IState idleState, IState stoppingState) {
        super(idleState);

        this.stoppingState = stoppingState;
    }

    @Override
    public boolean isStimulusEnabled(int elementID) {
        switch (elementID) {
            case UI_ELEMENT_STOP_CLASSIC_SCAN_BUTTON:
                return true;
            default:
                return super.isStimulusEnabled(elementID);
        }
    }

    @Override
    public void onStimulus(int elementID) {
        switch (elementID) {
            case UI_ELEMENT_STOP_CLASSIC_SCAN_BUTTON:
                stopAsync();
                break;

            default:
                super.onStimulus(elementID);
                break;
        }
    }

    @Override
    public void stopAsync() {
        super.stopAsync();
        if(engine != null) {
            engine.setState(stoppingState);
        }
    }

    @Override
    public void onClassicDiscovery(BluetoothDevice bluetoothDevice, int rssi) {
        if(engine != null && bluetoothDevice.getName() != null) {
            Device device = engine.deviceFromAddress(bluetoothDevice.getAddress());
            device.setRSSI(rssi);
            device.setName(bluetoothDevice.getName());
            engine.onDeviceChanged(device);
        }
    }

    @Override
    public void onClassicDiscoveryStopped() {
        if(engine != null)
            engine.setState(idleState);
    }

    @Override
    @StringRes
    public int getStatusText() {
        return R.string.classic_scan;
    }
}
