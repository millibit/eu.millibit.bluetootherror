package eu.millibit.bluetootherror.statemachine;

import android.bluetooth.BluetoothAdapter;
import android.support.annotation.NonNull;

import eu.millibit.bluetootherror.R;

class TurningAdaptorOff extends BaseState {
    private static final int OFF_TIME_MILLISECONDS = 2000;

    private final TurningAdaptorOn turningAdaptorOn;

    TurningAdaptorOff(IState idleState) {
        super(idleState);
        turningAdaptorOn = new TurningAdaptorOn(idleState);
    }

    @Override
    public int getStatusText() {
        return R.string.turning_adaptor_off;
    }

    @Override
    public void onEntry(@NonNull IEngine engine) {
        super.onEntry(engine);

        if(engine.getAdaptorState() == BluetoothAdapter.STATE_OFF)
            engine.setState(turningAdaptorOn);
        else
            engine.disableAdaptor();
    }

    @Override
    public void onAdaptorStateChanged(int newState) {
        super.onAdaptorStateChanged(newState);

        // Yes, we need to check if engine is null both inner and outer.
        // The runnable execute at a later time, so engine may be set to null
        // between queuing and execution.
        if(engine != null && newState == BluetoothAdapter.STATE_OFF) {
            engine.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(engine != null)
                        engine.setState(turningAdaptorOn);
                }
            }, OFF_TIME_MILLISECONDS);
        }
    }
}
