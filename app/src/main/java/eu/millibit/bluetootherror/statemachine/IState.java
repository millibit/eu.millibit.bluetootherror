package eu.millibit.bluetootherror.statemachine;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.support.annotation.StringRes;

public interface IState {
    static final int UI_ELEMENT_START_CLASSIC_SCAN_BUTTON = 1;
    static final int UI_ELEMENT_STOP_CLASSIC_SCAN_BUTTON = 2;
    static final int UI_ELEMENT_START_BLE_SCAN_BUTTON = 3;
    static final int UI_ELEMENT_STOP_BLE_SCAN_BUTTON = 4;
    static final int UI_ELEMENT_CONNECT_BUTTON = 5;
    static final int UI_ELEMENT_DISCONNECT_BUTTON = 6;
    static final int UI_ELEMENT_ADAPTOR_CYCLE_BUTTON = 7;

    boolean isStimulusEnabled(int elementID);

    boolean isConnected();

    @StringRes
    int getStatusText();

    void onEntry(IEngine engine);

    void onExit();

    void onStimulus(int elementID);

    void stopAsync();

    /* Bluetooth events */
    void onClassicDiscoveryStarted();

    void onClassicDiscoveryStopped();

    void onClassicDiscovery(BluetoothDevice bluetoothDevice, int rssi);

    void onBLEDiscovery(BluetoothDevice bluetoothDevice, int rssi);

    void onConnectionStateChange(BluetoothGatt gatt, int status, int newState);

    void onAdaptorStateChanged(int newState);
}

