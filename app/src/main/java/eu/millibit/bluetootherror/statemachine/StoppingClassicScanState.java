package eu.millibit.bluetootherror.statemachine;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import eu.millibit.bluetootherror.R;

public class StoppingClassicScanState extends BaseState {
    StoppingClassicScanState(IState idleState) {
        super(idleState);
    }

    @Override
    @StringRes
    public int getStatusText() {
        return R.string.stopping_classic_scan;
    }

    @Override
    public void onEntry(@NonNull IEngine engine) {
        super.onEntry(engine);

        engine.stopClassicScan();
    }

    @Override
    public void onClassicDiscoveryStopped() {
        if(engine != null)
            engine.setState(idleState);
    }
}
