package eu.millibit.bluetootherror.statemachine;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

public abstract class BaseState implements IState {
    final IState idleState;
    private static final String TAG = "BaseState";

    BaseState(IState idleState) {
        this.idleState = idleState;
    }

    @SuppressWarnings("WeakerAccess")
    @Nullable
    protected IEngine engine;

    @Override
    public boolean isStimulusEnabled(int elementID) {
        return false;
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    @Override
    public void stopAsync() {
    }

    @Override
    public void onEntry(@NonNull IEngine engine) {
        this.engine = engine;
    }

    @Override
    @CallSuper
    public void onExit() {
        engine = null;
    }

    @Override
    public void onStimulus(int elementID) {
    }

    @Override
    public void onClassicDiscoveryStarted() {
        Log.w(TAG, "Classic discovery started on its own");
    }

    @Override
    public void onClassicDiscoveryStopped() {
        Log.w(TAG, "Classic discovery stopped even though it's not running");
    }

    @Override
    public void onClassicDiscovery(BluetoothDevice bluetoothDevice, int rssi) {
        throw new IllegalStateException("This event shouldn't happen in this state");
    }

    @Override
    public void onBLEDiscovery(BluetoothDevice bluetoothDevice, int rssi) {
        throw new IllegalStateException("This event shouldn't happen in this state");
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        throw new IllegalStateException("This event shouldn't happen in this state");
    }

    @Override
    public void onAdaptorStateChanged(int newState) {
    }
}
