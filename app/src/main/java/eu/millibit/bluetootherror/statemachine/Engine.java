package eu.millibit.bluetootherror.statemachine;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import eu.millibit.bluetootherror.Device;

public class Engine  {

    IState idleState = new IdleState();

    public boolean isStimulusEnabled(int elementId) {
        return currentState.isStimulusEnabled(elementId);
    }

    public int getStatusText() {
        return currentState.getStatusText();
    }

    public boolean isConnected() {
        return currentState.isConnected();
    }

    public void onStimulus(int elementId) {
        currentState.onStimulus(elementId);
    }

    public interface IEngineDashboard {
        void onDeviceListCleared();
        void onDeviceListChanged();
        Device getSelectedDevice();
        void onAdapterStateChanged(int newState);
        void onStateChanged();
    }

    private final static String TAG = "Engine";

    @NonNull
    private final Handler handler;
    private final Context context;
    private final BluetoothAdapter bluetoothAdapter;
    private IEngineDashboard engineDashboard;
    private BluetoothLeScanner bleScanner;
    private BluetoothGatt bluetoothGatt;
    private IState currentState;
    private List<Device> devices = new LinkedList<>();

    public Engine(@NonNull Context context) throws IOException {
        this.context = context;

        handler = new Handler(Looper.getMainLooper());
        currentState = idleState;

        final BluetoothManager bluetoothManager =
                (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager != null) {
            bluetoothAdapter = bluetoothManager.getAdapter();
            if(bluetoothAdapter == null)
                throw new IOException("Can't get the BluetoothAdapter");
        } else {
            throw new IOException("Can't get the BluetoothManager");
        }

        globalBroadcastFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        globalBroadcastFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        globalBroadcastFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        globalBroadcastFilter.addAction(BluetoothDevice.ACTION_FOUND);

        idleState.onEntry(engineInterface);
    }

    private final ScanCallback bleScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);

            currentState.onBLEDiscovery(result.getDevice(), result.getRssi());
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            // FIXME: Do something logical here
            Log.e(TAG, "scan failed: " + errorCode);
        }
    };

    final private BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            Log.d(TAG, "onConnectionStateChange status=" + status + " newState=" + newState);
            // Use handler to switch to main thread. Callbacks from Bluetooth is normally delivered
            // on a dedicated thread.
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if(bluetoothGatt != null && bluetoothGatt == gatt)
                        currentState.onConnectionStateChange(gatt, status, newState);

                }
            });
        }
    };

    private final IntentFilter globalBroadcastFilter = new IntentFilter();

    private final BroadcastReceiver globalBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if(action == null)
                return;

            try {

                switch (action) {
                    case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
                        currentState.onClassicDiscoveryStarted();
                        break;

                    case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                        currentState.onClassicDiscoveryStopped();
                        break;

                    case BluetoothAdapter.ACTION_STATE_CHANGED:
                        final int newState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                        engineDashboard.onAdapterStateChanged(newState);
                        currentState.onAdaptorStateChanged(newState);
                        break;

                    case BluetoothDevice.ACTION_FOUND:
                        final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                        if (device.getType() == BluetoothDevice.DEVICE_TYPE_LE) {
                            final int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, (short) -999);
                            currentState.onClassicDiscovery(device, rssi);
                        }
                        break;
                }
            } catch (IllegalStateException exception) {
                Log.e(TAG, "Illegal event state for state " + currentState.toString());
                exception.printStackTrace();
            }
        }
    };

    public boolean isIdle() {
        return currentState == idleState;
    }


    public IEngineDashboard getEngineDashboard() {
        return engineDashboard;
    }

    public void setEngineDashboard(IEngineDashboard engineDashboard) {
        this.engineDashboard = engineDashboard;
        engineDashboard.onAdapterStateChanged(bluetoothAdapter.getState());
    }

    public List<Device> getDeviceList() {
        return devices;
    }

    public void onResume() {
        context.registerReceiver(globalBroadcastReceiver, globalBroadcastFilter);
    }

    public void onPause() {
        context.unregisterReceiver(globalBroadcastReceiver);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // IEngine interface
    ///////////////////////////////////////////////////////////////////////////////////////////////
    public IEngine engineInterface = new IEngine() {

        @Override
        public void startClassicScan() {
            // FIXME: Throw exception if not successfull
            bluetoothAdapter.startDiscovery();
        }

        @Override
        public void stopClassicScan() {
            // FIXME: Throw exception if not successfull
            bluetoothAdapter.cancelDiscovery();
        }

        @Override
        public void startBLEScan() {
            // FIXME: Throw exception if not successfull
            if (bluetoothAdapter != null) {
                if (bleScanner == null)
                    bleScanner = bluetoothAdapter.getBluetoothLeScanner();
                if (bleScanner != null) {
                    bleScanner.startScan(bleScanCallback);
                }
            }
        }

        @Override
        public void stopBLEScan() {
            // FIXME: Throw exception if not successfull
            if (bleScanner != null)
                bleScanner.stopScan(bleScanCallback);
        }

        @Override
        public void enableAdaptor() {
            if (bluetoothAdapter != null)
                bluetoothAdapter.enable();
        }

        @Override
        public void disableAdaptor() {
            if (bluetoothAdapter != null)
                bluetoothAdapter.disable();
        }

        @Override
        public int getAdaptorState() {
            if (bluetoothAdapter != null)
                return bluetoothAdapter.getState();
            else
                return BluetoothAdapter.STATE_OFF;
        }

        @Override
        public void clearDeviceList() {
            devices.clear();
            if (engineDashboard != null)
                engineDashboard.onDeviceListCleared();
        }

        @Override
        public void disconnect() {
            if (bluetoothGatt != null) {
                bluetoothGatt.disconnect();
            } else {
                Log.e(TAG, "Can't disconnect when we have no GATT");
            }
        }

        @Override
        public void disposeConnection() {
            Log.d(TAG, "Dispose connection");
            if (bluetoothGatt != null) {
                bluetoothGatt.close();
                bluetoothGatt = null;
            } else {
                Log.w(TAG, "Can't close non-existing connection");
            }
        }

        @Override
        public void connect(@NonNull String address, boolean autoConnect) {
            // FIXME: Throw exception if not successfull
            if (bluetoothGatt == null) {
                if (bluetoothAdapter != null) {
                    BluetoothDevice bluetoothDevice = bluetoothAdapter.getRemoteDevice(address);
                    bluetoothGatt = bluetoothDevice.connectGatt(context, autoConnect, gattCallback);

                    if (bluetoothGatt.connect()) {
                        Log.d(TAG, "Started connect to " + address);
                    } else {
                        Log.w(TAG, "bluetoothGatt.connect() failed");
                        bluetoothGatt.close();
                        bluetoothGatt = null;
                    }
                }
            } else {
                Log.e(TAG, "Can't start a connection while the old connection is not disposed");
            }
        }

        @Override
        public void setState(IState newState) {
            Log.d(TAG, currentState.toString() + " -> " + newState.toString());
            currentState.onExit();
            currentState = newState;
            if (engineDashboard != null)
                engineDashboard.onStateChanged();
            newState.onEntry(this);
        }

        @NonNull
        public synchronized Device deviceFromAddress(@NonNull String address) {
            for (Device device : devices)
                if (address.equals(device.getAddress()))
                    return device;

            // Not found - we'll create it
            final Device device = new Device(address);
            devices.add(device);
            if (engineDashboard != null)
                engineDashboard.onDeviceListChanged();

            return device;
        }

        @Nullable
        @Override
        public Device getSelectedDevice() {
            if (engineDashboard != null)
                return engineDashboard.getSelectedDevice();
            else
                return null;
        }

        @Override
        public void onDeviceChanged(@NonNull Device device) {
            if (engineDashboard != null)
                engineDashboard.onDeviceListChanged();
        }

        @Override
        public void postDelayed(Runnable r, int milliseconds) {
            handler.postDelayed(r, milliseconds);
        }
    };
}
