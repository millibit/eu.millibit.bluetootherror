package eu.millibit.bluetootherror;

public class Device {
    private String name;
    private final String address;
    private int rssi;

    public Device(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        String name = this.name;
        if (name == null)
            name = "<unnamed>";

        return name + " (" + address + ") " + rssi + " dBm";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setRSSI(int rssi) {
        this.rssi = rssi;
    }

    public int getRSSI() {
        return rssi;
    }

    public String getAddress() {
        return address;
    }
}
