package eu.millibit.bluetootherror.blueframework;

public interface IBluetoothController {
    public interface IObserver {
        void onBluetoothScanStopped();
        void onBluetoothScanStarted();
        void onBluetoothConnected(String address);
        void onBluetoothDisconnected(String address);
        void onBluetoothAdapterEnabled();
        void onBluetoothAdapterDisabled();
    }
}
