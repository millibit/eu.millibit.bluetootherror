package eu.millibit.bluetootherror.blueframework;

public interface ITestActivity {
    void onStart(IBluetoothController bluetoothController);
    void onStopRequest();
    String getName();
}
