package eu.millibit.bluetootherror;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class DeviceListAdapter extends ArrayAdapter<Device> {
    @NonNull
    private final Activity activity;

    @NonNull
    private final List<Device> devices;

    private boolean connected;

    private int selectedIndex = -1;

    DeviceListAdapter(@NonNull Activity activity, @NonNull List<Device> devices) {
        super(activity, R.layout.device_list_item);
        this.activity = activity;
        this.devices = devices;
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    void setSelectedIndex(int selectedIndex) {
        if(this.selectedIndex != selectedIndex) {
            this.selectedIndex = selectedIndex;
            notifyDataSetChanged();
        }
    }

    int getSelectedIndex() {
        return selectedIndex;
    }

    @SuppressLint("InflateParams")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View rowView;
        if(convertView != null)
            rowView = convertView;
        else {
            LayoutInflater inflater = activity.getLayoutInflater();
            rowView = inflater.inflate(R.layout.device_list_item, null, true);
        }

        TextView deviceDescription = rowView.findViewById(R.id.device_description);
        Device device = devices.get(position);
        deviceDescription.setText(device.toString());

        ImageView imageViewSelected = rowView.findViewById(R.id.device_selected);
        ImageView imageViewSelectedConnected = rowView.findViewById(R.id.device_selected_connected);

        if(position == selectedIndex) {
            if(connected) {
                imageViewSelected.setVisibility(View.INVISIBLE);
                imageViewSelectedConnected.setVisibility(View.VISIBLE);
            } else {
                imageViewSelected.setVisibility(View.VISIBLE);
                imageViewSelectedConnected.setVisibility(View.INVISIBLE);

            }
        } else {
            imageViewSelected.setVisibility(View.INVISIBLE);
            imageViewSelectedConnected.setVisibility(View.INVISIBLE);
        }


        return rowView;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        if(this.connected != connected) {
            this.connected = connected;
            notifyDataSetChanged();
        }
    }
}
