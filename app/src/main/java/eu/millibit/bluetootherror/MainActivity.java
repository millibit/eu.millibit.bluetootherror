package eu.millibit.bluetootherror;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

import eu.millibit.bluetootherror.statemachine.Engine;
import eu.millibit.bluetootherror.statemachine.IState;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    // UI elements
    Button startClassicScanButton;
    Button stopClassicScanButton;
    Button startBLEScanButton;
    Button stopBLEScanButton;
    Button connectButton;
    Button disconnectButton;
    Button cycleAdapterButton;
    TextView statusTextView;
    TextView adaptorStatusView;

    Engine engine;

    DeviceListAdapter deviceListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startClassicScanButton = findViewById(R.id.startClassicScanButton);
        stopClassicScanButton = findViewById(R.id.stopClassicScanButton);
        startBLEScanButton = findViewById(R.id.startBLEScanButton);
        stopBLEScanButton = findViewById(R.id.stopBLEScanButton);
        connectButton = findViewById(R.id.connectButton);
        disconnectButton = findViewById(R.id.disconnectButton);
        statusTextView = findViewById(R.id.statusTextView);
        cycleAdapterButton = findViewById(R.id.cycleAdapterButton);
        adaptorStatusView = findViewById(R.id.adapterStatus);


        try {
            engine = new Engine(this);
            engine.setEngineDashboard(engineUserInterface);
        } catch (IOException exception) {
            Log.e(TAG, "Error creating engine: " + exception.getMessage());
        }

        deviceListAdapter = new DeviceListAdapter(this, engine.getDeviceList());
        ListView listView = findViewById(R.id.scanResultList);
        if(listView != null) {
            listView.setAdapter(deviceListAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    if(engine.isIdle())
                        deviceListAdapter.setSelectedIndex(i);
                }
            });
        }

        updateUIFromCurrentState();
        askLocationPermission();
    }

    Engine.IEngineDashboard engineUserInterface = new Engine.IEngineDashboard() {
        @Override
        public void onDeviceListChanged() {
            deviceListAdapter.notifyDataSetChanged();
        }

        @Override
        public void onDeviceListCleared() {
            deviceListAdapter.setSelectedIndex(-1);
            deviceListAdapter.notifyDataSetChanged();
        }

        @Override
        public Device getSelectedDevice() {
            int index = deviceListAdapter.getSelectedIndex();
            List<Device> devices = engine.getDeviceList();

            if (index >= 0 && index < devices.size())
                return devices.get(index);
            else
                return null;
        }

        @Override
        public void onAdapterStateChanged(int newState) {
            switch (newState) {
                case BluetoothAdapter.STATE_ON:
                    adaptorStatusView.setText(R.string.enabled);
                    break;

                case BluetoothAdapter.STATE_OFF:
                    adaptorStatusView.setText(R.string.disabled);
                    break;

                case BluetoothAdapter.STATE_TURNING_OFF:
                    adaptorStatusView.setText(R.string.turning_off);
                    break;

                case BluetoothAdapter.STATE_TURNING_ON:
                    adaptorStatusView.setText(R.string.turning_on);
                    break;

                default:
                    adaptorStatusView.setText(R.string.unknown_state);
                    break;
            }}

        @Override
        public void onStateChanged() {
            updateUIFromCurrentState();
        }
    };


    private void showAppSettings() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        startActivity(intent);
    }

    private void askLocationPermission() {
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
            dlgAlert.setMessage("Permission to determine your coarse location is necessary to make Bluetooth work.\n"
                    + "Open settings, select permissions and enable \"Location\"");
            dlgAlert.setTitle(R.string.app_name);
            dlgAlert.setCancelable(true);
            dlgAlert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            dlgAlert.setNeutralButton(R.string.open_settings, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    showAppSettings();
                }
            });
            dlgAlert.create().show();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_REQUEST_LOCATION);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        engine.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        engine.onPause();
    }

    private final static int PERMISSION_REQUEST_LOCATION = 1;

    private void updateEnabled(@Nullable View view, int elementId) {
        if(view != null)
            view.setEnabled(engine.isStimulusEnabled(elementId));
    }

    private void updateUIFromCurrentState() {

        updateEnabled(startClassicScanButton, IState.UI_ELEMENT_START_CLASSIC_SCAN_BUTTON);
        updateEnabled(stopClassicScanButton, IState.UI_ELEMENT_STOP_CLASSIC_SCAN_BUTTON);
        updateEnabled(startBLEScanButton, IState.UI_ELEMENT_START_BLE_SCAN_BUTTON);
        updateEnabled(stopBLEScanButton, IState.UI_ELEMENT_STOP_BLE_SCAN_BUTTON);
        updateEnabled(connectButton, IState.UI_ELEMENT_CONNECT_BUTTON);
        updateEnabled(disconnectButton, IState.UI_ELEMENT_DISCONNECT_BUTTON);
        updateEnabled(cycleAdapterButton, IState.UI_ELEMENT_ADAPTOR_CYCLE_BUTTON);

        if(statusTextView != null)
            statusTextView.setText(engine.getStatusText());

        deviceListAdapter.setConnected(engine.isConnected());
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Handlers from UI
    ///////////////////////////////////////////////////////////////////////////////////////////////
    public void startClassicScan(View view) {
        engine.onStimulus(IState.UI_ELEMENT_START_CLASSIC_SCAN_BUTTON);
    }

    public void stopClassicScan(View view) {
        engine.onStimulus(IState.UI_ELEMENT_STOP_CLASSIC_SCAN_BUTTON);
    }

    public void startBLEScan(View view) {
        engine.onStimulus(IState.UI_ELEMENT_START_BLE_SCAN_BUTTON);
    }

    public void stopBLEScan(View view) {
        engine.onStimulus(IState.UI_ELEMENT_STOP_BLE_SCAN_BUTTON);
    }

    public void connect(View view) {
        engine.onStimulus(IState.UI_ELEMENT_CONNECT_BUTTON);
    }

    public void disconnect(View view) {
        engine.onStimulus(IState.UI_ELEMENT_DISCONNECT_BUTTON);
    }

    public void cycleAdapter(View view) {
        engine.onStimulus(IState.UI_ELEMENT_ADAPTOR_CYCLE_BUTTON);
    }
}
